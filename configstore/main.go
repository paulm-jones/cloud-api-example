package configstore

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"cloud-api-prototype/gitwrapper"
	"github.com/go-git/go-billy/v5"
)

// AddRuleCollection adds or replaces a rule collection file with a name matching
// the 'name' property in the rule Collection
func AddRuleCollection(w http.ResponseWriter, r *http.Request) {

	repo, fs, err := gitwrapper.Clone(gitURL())
	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte(fmt.Sprintf("Failed to clone a configuration repo: %v", err)))
		return
	}

	ruleCollection, _ := parseRuleCollection(r)
	filename, err := createRuleCollectionFile(fs, ruleCollection)
	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte(fmt.Sprintf("Failed to create Rule Collection file %v: %v", filename, err)))
		return
	}

	err = gitwrapper.Commit(repo, filename)
	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte(fmt.Sprintf("Failed to commit %v to repo: %v", filename, err)))
		return
	}

	err = gitwrapper.Push(repo, auth())
	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte(fmt.Sprintf("Failed to push to remote: %v", err)))
		return
	}

	w.WriteHeader(200)
	j, _ := json.Marshal(ruleCollection)
	w.Write(j)

}

func parseRuleCollection(r *http.Request) (rc ruleCollection, err error) {

	b, _ := ioutil.ReadAll(r.Body)
	err = json.Unmarshal(b, &rc)
	if err != nil {
		log.Printf("Failed to unmarshal Rule Collection input: %v", err)
	}
	return
}

func createRuleCollectionFile(fs billy.Filesystem, rc ruleCollection) (filename string, err error) {

	filename = rc.Name + ".json"

	f, err := fs.Create(filename)
	if err != nil {
		log.Printf("Failed to create file %v: %v", filename, err)
		return
	}

	j, err := json.Marshal(rc)
	if err != nil {
		log.Printf("Failed to marshal JSON contents of Rule Collection: %v", err)
		return
	}

	_, err = f.Write(j)
	if err != nil {
		log.Printf("Failed to write contents of Rule Collection file %v: %v", filename, err)
		return
	}

	return
}
