package configstore

// rule represents an individual Firewall rule within a rule Collection
type rule struct {
	Name   string `json:"name"`
	Action string `json:"action"`
	Port   int16  `json:"port"`
}

// ruleCollection represents an Azure Firewall rule Collection that contains one or more Rules
type ruleCollection struct {
	Name  string `json:"name"`
	Rules []rule `json:"rules"`
}
