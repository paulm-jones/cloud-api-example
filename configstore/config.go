package configstore

import (
	"os"

	gohttp "github.com/go-git/go-git/v5/plumbing/transport/http"
)

func gitURL() string {
	return "https://gitlab.com/paulm-jones/cloud-api-config"
}

func auth() *gohttp.BasicAuth {
	return &gohttp.BasicAuth{
		Username: os.Getenv("GITLAB_USER"),
		Password: os.Getenv("GITLAB_TOKEN"),
	}

}
