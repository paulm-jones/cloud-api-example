package gitwrapper

import (
	"fmt"
	"log"
	"time"

	"github.com/go-git/go-billy/v5"
	"github.com/go-git/go-billy/v5/memfs"
	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing/object"
	"github.com/go-git/go-git/v5/plumbing/transport/http"
	"github.com/go-git/go-git/v5/storage/memory"
)

// Clone creates and returns a reference to an in-memory instance of a Git repository
func Clone(url string) (r *git.Repository, fs billy.Filesystem, err error) {

	s := memory.NewStorage()
	fs = memfs.New()

	r, err = git.Clone(s, fs, &git.CloneOptions{
		URL:               url,
		RecurseSubmodules: git.DefaultSubmoduleRecursionDepth,
	})

	return
}

// Commit adds a given file to a repository and then commits it
func Commit(repo *git.Repository, filename string) error {

	wt, err := repo.Worktree()
	if err != nil {
		log.Printf("Couldn't get a worktree: %v ", err)
		return err
	}

	_, err = wt.Add(filename)
	if err != nil {
		log.Printf("Couldn't add %v to the worktree: %v", filename, err)
		return err
	}

	// Todo make commit message more generic and pull author from auth
	_, err = wt.Commit(fmt.Sprintf("%v Rule Collection added by API", filename), &git.CommitOptions{
		Author: &object.Signature{
			Name:  "John Doe",
			Email: "john@doe.org",
			When:  time.Now(),
		},
	})
	if err != nil {
		log.Printf("Couldn't commit: %v", err)
		return err
	}

	return nil
}

// Push pushes to a remote called origin
func Push(repo *git.Repository, auth *http.BasicAuth) (err error) {

	err = repo.Push(&git.PushOptions{
		RemoteName: "origin",
		Auth:       auth,
	})
	if err != nil {
		fmt.Println("Failed to push")
		return
	}

	fmt.Println("Remote updated")
	return

}
